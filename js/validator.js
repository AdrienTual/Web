/**
 * Created by Adrien on 18/11/2016.
 */
var submit = true;
$(document).ready(function() {
    $('#professorForm').bootstrapValidator({
        message: 'Cette valeur n&apos;est pas valide',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            PEmail: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre email'
                    },
                    emailAddress: {
                        message: 'Entrez un email valide'
                    }
                }
            },
            PfirstName: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre prenom'
                    },
                    stringLength: {
                        min: 2
                    }
                }
            },
            PlastName: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre nom'
                    },
                    stringLength: {
                        min: 2
                    }
                }
            },
            Ppassword: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre mot de passe'
                    },
                    identical: {
                        field: 'PconfirmPassword',
                        message: 'Le mot de passe et sa confirmation doivent être identiques'
                    }
                }
            },
            PconfirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre mot de passe de nouveau'
                    },
                    identical: {
                        field: 'Ppassword',
                        message: 'Le mot de passe et sa confirmation doivent être identiques'
                    }
                }
            }
        }
    });
});

$(document).ready(function() {
    $('#studentForm').bootstrapValidator({
        message: 'Cette valeur n&apos;est pas valide',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            SEmail: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre email'
                    },
                    emailAddress: {
                        message: 'Entrez un email valide'
                    }
                }
            },
            SfirstName: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre prenom'
                    },
                    stringLength: {
                        min: 2
                    }
                }
            },
            SlastName: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre nom'
                    },
                    stringLength: {
                        min: 2
                    }
                }
            },
            Spassword: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre mot de passe'
                    },
                    identical: {
                        field: 'SconfirmPassword',
                        message: 'Le mot de passe et sa confirmation doivent être identiques'
                    }
                }
            },
            SconfirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'Entrez votre mot de passe de nouveau'
                    },
                    identical: {
                        field: 'Spassword',
                        message: 'Le mot de passe et sa confirmation doivent être identiques'
                    }
                }
            }
        }
    });
});

$(document).ready(function() {
    var x_timer;
    $("#PEmail").keyup(function (e){
        clearTimeout(x_timer);
        var user_name = $(this).val();
        x_timer = setTimeout(function(){
            check_username_ajax(user_name);
        }, 1000);
    });

    function check_username_ajax(username){
        $("#Pemail-result").html('<img src="./images/ajax-loader.gif"/>');
        $.post('treatment/email_validation.php', {'username':username}, function(data) {
            if(data == 'VALID'){
                $("#Pemail-result").html('');
            }
            else if (data == 'INVALID'){
                $("#Pemail-result").html('Email déjà utilisé');
                $('#professorForm').data('bootstrapValidator').updateStatus('PEmail', 'INVALID', null);
                $('small[data-bv-for="PEmail"]').html('');
            }
        });
    }
});

$(document).ready(function() {
    var x_timer;
    $("#SEmail").keyup(function (e){
        clearTimeout(x_timer);
        var user_name = $(this).val();
        x_timer = setTimeout(function(){
            check_username_ajax(user_name);
        }, 1000);
    });

    function check_username_ajax(username){
        $("#Semail-result").html('<img src="./images/ajax-loader.gif"/>');
        $.post('treatment/email_validation.php', {'username':username}, function(data) {
            if(data == 'VALID'){
                $("#Semail-result").html('');
            }
            else if (data == 'INVALID'){
                $("#Semail-result").html('Email déjà utilisé');
                $('#studentForm').data('bootstrapValidator').updateStatus('SEmail', 'INVALID', null);
                $('small[data-bv-for="SEmail"]').html('');
            }
        });
    }
});

$(document).ready(function() {
    $('#studentForm').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        if (submit){
            submit = false;
            var typev = 'student';
        var emailv = $('#SEmail').val();
        var firstNamev = $('#SfirstName').val();
        var lastNamev = $('#SlastName').val();
        var pwv = $('#Spassword').val();
        var pwconfirmv = $('#SconfirmPassword').val();

        $.post('treatment/email_sender.php',{email:emailv,firstName:firstNamev,lastName:lastNamev,pw:pwv,pwconfirm:pwconfirmv,type:typev},function(data){
            if (data == 'OK'){
                $('#Sconfirmmsg').html('Un Email de confirmation viens de vous être envoyé, consultez votre boite mail');
            }
            submit = true;
            }
        );}
    });

});

$(document).ready(function() {
    $('#professorForm').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        if (submit){
            submit = false;
            var typev = 'professor';
            var emailv = $('#PEmail').val();
            var firstNamev = $('#PfirstName').val();
            var lastNamev = $('#PlastName').val();
            var pwv = $('#Ppassword').val();
            var pwconfirmv = $('#PconfirmPassword').val();

            $.post('treatment/email_sender.php',{email:emailv,firstName:firstNamev,lastName:lastNamev,pw:pwv,pwconfirm:pwconfirmv,type:typev},function(data){
                    if (data == 'OK'){
                        $('#Pconfirmmsg').html('Un Email de confirmation viens de vous être envoyé, consultez votre boite mail');
                    }
                    submit = true;
                }
            );}
    });

});
