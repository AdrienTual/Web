<?php
/**
 * Created by PhpStorm.
 * User: Adrien
 * Date: 20/11/2016
 * Time: 17:34
 */

$bdd = require_once ('../includes/bddpdo.inc.php');

if(isset($_GET['email'], $_GET['key']) AND !empty($_GET['email']) AND !empty($_GET['key'])) {
    $email = htmlspecialchars(urldecode($_GET['email']));
    $key = htmlspecialchars($_GET['key']);
    $requser = $bdd->prepare("SELECT * FROM users WHERE Email = ? AND confirmkey = ?");
    $requser->execute(array($email, $key));
    $userexist = $requser->rowCount();
    if($userexist == 1) {
        $user = $requser->fetch();
        if($user['confirm'] == 0) {
            $updateuser = $bdd->prepare("UPDATE users SET confirm = 1 WHERE Email = ? AND confirmkey = ?");
            $updateuser->execute(array($email,$key));
            echo "Votre compte a bien été confirmé ! <a href='../index.php'>revenir à l'accueil</a>";
        } else {
            echo "Votre compte a déjà été confirmé ! <a href=\"../index.php\">revenir à l'accueil</a>";
        }
    } else {
        echo "L'utilisateur n'existe pas ! <a href='../index.php'>revenir à l'accueil</a>";
    }
}