<?php
/**
 * Created by PhpStorm.
 * User: Adrien
 * Date: 19/11/2016
 * Time: 23:12
 */

if(isset($_POST["email"]) && isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["pw"]) && isset($_POST["pwconfirm"]) && isset($_POST['type'])){
    $conn = require_once('../includes/bddpdo.inc.php');
    $email = htmlspecialchars($_POST['email']);
    $firstName = htmlspecialchars($_POST['firstName']);
    $lastName = htmlspecialchars($_POST['lastName']);
    $type = $_POST['type'];
    $pw = sha1($_POST['pw']);
    $pwconfirm = sha1($_POST['pwconfirm']);
    if(!empty($_POST['email']) AND !empty($_POST['firstName']) AND !empty($_POST['lastName']) AND !empty($_POST['pw']) AND !empty($_POST['pwconfirm'])) {
        $emailLength = strlen($email);
        if($emailLength <= 255) {
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $statement = $conn->prepare("SELECT Email FROM users WHERE Email=?");
                $statement->execute(array($email));
                $nb = $statement-> rowCount();
                $statement->closeCursor();
                if($nb == 0) {
                    if($pw == $pwconfirm) {
                        $longueurKey = 15;
                        $key = "";
                        for($i=1;$i<$longueurKey;$i++) {
                            $key .= mt_rand(0,9);
                        }
                        $nb = 0;
                        $bool = 0;
                        $insertmbr = $conn->prepare("INSERT INTO users(Email ,password ,Number_Visite ,firstname ,lastname , typeuser, confirm ,confirmKey) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                        $insertmbr->execute(array($email, $pw, $nb, $firstName, $lastName, $type, $bool, $key));

                        $conn = null;

                        $header="MIME-Version: 1.0\r\n";
                        $header.='From:"ludomaths.esy.es"<no-reply@ludomaths.esy.es>'."\n";
                        $header.='Content-Type:text/html; charset="uft-8"'."\n";
                        $header.='Content-Transfer-Encoding: 8bit';

                        $message='
                     <html>
                        <body>
                           <div align="center">
                              <a href="http://ludomaths.esy.es/treatment/confirmation.php?email='.urlencode($email).'&key='.$key.'">Confirmez votre compte !</a>
                           </div>
                        </body>
                     </html>
                     ';
                        mail($email, "Confimation par mail", $message, $header);
                        die ('OK');
                    } else {
                        $erreur = "Vos mots de passes ne correspondent pas !";
                        die($erreur);
                    }
                } else {
                    $erreur = "Adresse mail déjà utilisée !";
                    die($erreur);
                }
            } else {
                $erreur = "Votre adresse mail n'est pas valide !";
                die($erreur);
            }
        } else {
            $erreur = "Votre pseudo ne doit pas dépasser 255 caractères !";
            die($erreur);
        }
    } else {
        $erreur = "Tous les champs doivent être complétés !";
        die($erreur);
    }
}
