<?php

$hote = $_SERVER['HTTP_HOST'];
$page = '';
$and = '?';
if(isset($_GET['page'])){
    $page = '?page='.$_GET['page'].'';
}
if(isset($_POST['emailc']) && isset($_POST['pwc'])){
    if(!empty($_POST['emailc']) AND !empty($_POST['pwc'])){
        $email = htmlspecialchars($_POST['emailc']);
        $pw = sha1($_POST['pwc']);
        $conn = require_once('../includes/bddpdo.inc.php');
        $statement = $conn->prepare("SELECT * FROM users WHERE Email=? AND password=?");
        $statement->execute(array($email,$pw));
        $res = $statement->fetch();
        $nb = $statement->rowCount();
        if($nb == 1){
            if ($res['confirm'] == '1'){
                $emailsession = $res['Email'];
                $firstnamesession = $res['firstname'];
                $lastnamesession = $res['lastname'];
                $typeuser = $res['typeuser'];

                $statement->closeCursor();
                session_start();
                $_SESSION['email'] = $emailsession;
                $_SESSION['firstname'] = $firstnamesession;
                $_SESSION['lastname'] = $lastnamesession;
                $_SESSION['typeuser'] = $typeuser;
                header('location: http://'.$hote.'/index.php'.$page.'');
            }
            else{
                if(isset($_GET['page'])){
                    $and = '&';
                }
                header('location: http://'.$hote.'/index.php'.$page.$and.'errc=1');
            }
        }
        else{
            if(isset($_GET['page'])){
                $and = '&';
            }
            header('location: http://'.$hote.'/index.php'.$page.$and.'errv=1');
        }
    }
}