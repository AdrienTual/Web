<?php
/**
 * Created by PhpStorm.
 * User: Adrien
 * Date: 19/11/2016
 * Time: 01:52
 */

if(isset($_POST["username"]))
{
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        die();
    }

    $conn = require_once('../includes/bdd.inc.php');

    $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);

    $statement = $conn->prepare("SELECT Email FROM users WHERE Email=?");
    $statement->bind_param('s', $username);
    $statement->execute();
    $statement->bind_result($username);
    if($statement->fetch()){
        die('INVALID');
    }else{
        die('VALID');
    }
}