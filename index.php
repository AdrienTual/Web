<?php
session_start();

require_once ('./includes/util.php');
/* On définit une liste de pages autorisées au moyen d'un tableau
 Vous devrez ajouter toutes les pages se trouvant dans le répertoire
 pages pour les rendre accessibles.
 Vous pouvez également automatiser la génération de ce tableau
 en lisant le contenu du répertoire pages */
$pagesOk = array('home','CP_Chapitre1', 'CP_Chapitre2');

// On commence par lire la page demandée
// Si ce n'est pas vide
if(!empty($_GET['page']))
    // On lit la valeur demandée
    $page = $_GET['page'];
else
    // Sinon on utiliser home comme page par défaut
    $page = 'home';

// On vérifie que la page demandée se trouve dans les pages autorisées
if(!in_array($page,$pagesOk))
{

    // Elle n'y est pas, dans ce cas on peut renvoyer une erreur 404
    header("HTTP/1.0 404 Not Found");

    // On stop l'exécution du script ici
    exit;

}


// La construction de la page.
// On commence par inclure l'entête
require('./includes/header.php');

//On inclut la page demandée
require('./pages/'.$page.'.php');

// On inclut enfin le pied de page
require('./includes/footer.php');

?>

