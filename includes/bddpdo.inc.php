<?php
/**
 * Created by PhpStorm.
 * User: Adrien
 * Date: 20/11/2016
 * Time: 18:47
 */

$config = @require_once("../config.php"); //Changer le fichier "config.php" pour modifier l'accès aux bdd
$valeur = "LUDO";
$servername = "DB_HOST";
$dbname = "DB_NAME";
$username = "DB_USER";
$password = "DB_PASSWORD";

$connexion = $config[$valeur];

$conn = new PDO('mysql:host='.$connexion[$servername].';dbname='.$connexion[$dbname], $connexion[$username], $connexion[$password]);
if ($conn->connect_error) {
    die(1);
}


return $conn;