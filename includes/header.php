<?//php session_start();?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/style.css" type="text/css" />

    <title>
        <?php
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }
        else{
            $page = "home";
        }
            switch($page){
                case "home":
                    echo "Accueil";
                    break;
                default:
                    echo "home";
                    break;
            }
        ?>
</title>
</head>
<body>
<header class="row">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="./images/logo.png" width=150% alt="logo"></a>
            </div>
            <div id="connexionAccount">
                <?php
                $error = '';
                if(isset($_GET['errv'])){
                    $error = 'L\'email ou le mot de passe est erroné';
                }
                else if(isset($_GET['errc'])){
                    $error = 'Le compte n\'est pas confirmé';
                }
                if(isset($_SESSION['email']) && isset($_SESSION['firstname']) && isset($_SESSION['typeuser'])){
                    echo'<ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong>'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'</strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span class="glyphicon glyphicon-user icon-size"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong>'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'</strong></p>
                                        <p class="text-left small">'.$_SESSION['email'].'</p>
                                        <p class="text-left">
                                            <a href="#" class="btn btn-primary btn-block btn-sm">Voir le profil</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>';
                    if ($_SESSION['typeuser'] == 'student'){
                        echo'
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <a href="#">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="#">
                                                Ma progression<span class="glyphicon glyphicon-stats pull-right">
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>';}
                        else if ($_SESSION['typeuser'] == 'professor'){
                            echo'
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <a href="#">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="#">
                                                Gérer mes classes<span class="glyphicon glyphicon-briefcase pull-right">
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>';
                        }
                    echo '
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="treatment/deconnexion.php" class="btn btn-danger btn-block">Deconnexion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>';
                }
                else{
                    echo '<div id="navbar" class="navbar-collapse collapse">
                    <form id="formConnexion" class="navbar-form navbar-right" method="post" action="treatment/connexion.php">
                        <div class="form-group">
                            <label for="emailc" style="color: rgba(195, 0, 0, 0.75)" id="connexionresult">' .$error.'     </label>
                            <input type="text" placeholder="Email" name="emailc" id="emailc" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Password" name="pwc" id="pwc" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-success">Se connecter</button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-md">S\'inscrire</button>
                    </form>
                </div><!--/.navbar-collapse -->';
                }
                ?>
            </div>
        </div>
    </nav>
</header>


<!-- Mod   -->
<div class="modal fade bs-modal-md" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <br>
            <div class="bs-example bs-example-tabs">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#student" data-toggle="tab">Eleve</a></li>
                    <li class=""><a href="#professor" data-toggle="tab">Professeur</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="student">
                        <form class="form-horizontal" id="studentForm">
                            <fieldset>
                                <!-- Sign Up Form -->
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="SEmail">Email</label>
                                    <div class="col-md-8">
                                        <input id="SEmail" name="SEmail" class="form-control" type="text" placeholder="example@mail.com" class="input-large" required="">
                                        <small class="help-block" id="Semail-result"></small>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="SfirstName">Prenom</label>
                                    <div class="col-md-8">
                                        <input id="SfirstName" name="SfirstName" class="form-control" type="text" placeholder="Jean" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="SlastName">Nom</label>
                                    <div class="col-md-8">
                                        <input id="SlastName" name="SlastName" class="form-control" type="text" placeholder="Dupont" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Password input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="Spassword">Mot de passe</label>
                                    <div class="col-md-8">
                                        <input id="Spassword" name="Spassword" class="form-control" type="password" placeholder="********" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="SconfirmPassword">Confirmation</label>
                                    <div class="col-md-8">
                                        <input id="SconfirmPassword" class="form-control" name="SconfirmPassword" type="password" placeholder="********" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="Sconfirmsignup" id="Sconfirmmsg"></label>
                                    <div class="col-md-3">
                                        <button type="submit" name="Sconfirmsignup" class="btn btn-success">S'inscrire</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="professor">
                        <form method="post" class="form-horizontal" id="professorForm" action="treatment/email_sender.php">
                            <fieldset>
                                <!-- Sign Up Form -->
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="PEmail">Email</label>
                                    <div class="col-md-8">
                                        <input id="PEmail" name="PEmail" class="form-control" type="text" placeholder="example@mail.com" class="input-large" required="">
                                        <small class="help-block" id="Pemail-result"></small>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="PfirstName">Prenom</label>
                                    <div class="col-md-8">
                                        <input id="PfirstName" name="PfirstName" class="form-control" type="text" placeholder="Jean" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="PlastName">Nom</label>
                                    <div class="col-md-8">
                                        <input id="PlastName" name="PlastName" class="form-control" type="text" placeholder="Dupont" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Password input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="Ppassword">Mot de passe</label>
                                    <div class="col-md-8">
                                        <input id="Ppassword" name="Ppassword" class="form-control" type="password" placeholder="********" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="PconfirmPassword">Confirmation</label>
                                    <div class="col-md-8">
                                        <input id="PconfirmPassword" class="form-control" name="PconfirmPassword" type="password" placeholder="********" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="Pconfirmsignup" id="Pconfirmmsg"></label>
                                    <div class="col-md-3">
                                        <button type="submit" name="Pconfirmsignup" class="btn btn-success">S'inscrire</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </center>
            </div>
        </div>
    </div>
</div>
