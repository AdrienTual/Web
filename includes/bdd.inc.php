<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 05/11/2016
 * Time: 14:25
 */

$config = @require_once("../config.php"); //Changer le fichier "config.php" pour modifier l'accès aux bdd
$valeur = "LUDO";
$servername = "DB_HOST";
$dbname = "DB_NAME";
$username = "DB_USER";
$password = "DB_PASSWORD";

$connexion = $config[$valeur];

$conn = new mysqli($connexion[$servername], $connexion[$username], $connexion[$password]);
if ($conn->connect_error) {
    die(1);
}

$conn->select_db($connexion[$dbname]);

return $conn;
