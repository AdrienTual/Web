<?php
/**
 * Created by PhpStorm.
 * User: Adrien
 * Date: 19/11/2016
 * Time: 19:08
 *
 * Fichier pour les futurs fonctions à implementer
 */

function getURI(){
    $adresse = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
    $i = 0;
    foreach($_GET as $cle => $valeur){
        $adresse .= ($i == 0 ? '?' : '&').$cle.($valeur ? '='.$valeur : '');
        $i++;
    }
    return $adresse;
}