import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Morpion extends Applet implements ActionListener{
	Button cases[];
	Button newGame ; 
	Label score; 
	int casesFree = 0;

	public void init(){
		this.setLayout(new BorderLayout());
		this.setBackground(Color.RED);
		newGame = new Button("Nouvelle Partie");
		newGame.addActionListener(this);

		Panel sup = new Panel();
		sup.add(newGame);
		this.add(sup,"North");

		Panel center = new Panel();
		center.setLayout(new GridLayout(3,3));
		this.add(center,"North");

		score = new Label("Let's go");
		this.add(score,"South");

		cases = new Button[9];

		for(int index = 0 ; index <9 ; index ++){
			cases[index] = new Button();
			cases[index].addActionListener(this);
			cases[index].setBackground(Color.CYAN);
			center.add(cases[index]);
		}


	}
	public void actionPerformed(ActionEvent event){
		Button button = (Button) event.getSource();
		if(button == newGame){
			for(int index = 0 ; index <9 ; index ++){
				cases[index].setEnabled(true);
				cases[index].setLabel("");
				cases[index].setBackground(Color.CYAN);
			}
			casesFree = 9 ; 
			score.setText("Let's go");
			newGame.setEnabled(true);
			return;
		}
		String winner = "";

		for(int index = 0 ; index <9 ; index ++){
			if(button == cases[index]){
				cases[index].setLabel("X");
				winner = whoIsWinner();

				if(!"".equals(winner)){
					finishGame();
				}
				else{
					gameComputer();
					winner = whoIsWinner();
					if(!"".equals(winner)){
						finishGame();
					}
				}break;
			}
		}

		if(winner.equals("X")){
			score.setText("Vous avez gagné!");
		}
		else if(winner.equals("0")){
			score.setText("Vous avez perdu!");
		}
		else if(winner.equals("T")){
			score.setText("Egalité");
		}

	}
	private void gameComputer() {
		int caseSelected;
		caseSelected = findCaseEmpty("0");
		if(caseSelected == -1){
			caseSelected = findCaseEmpty("0");
		}
		if((caseSelected == -1)&&(cases[4].getLabel().equals(""))){
			caseSelected = 4;
		}
		if(caseSelected == -1){
			caseSelected = caseAuHasard("");
		}
		cases[caseSelected].setLabel("0");
	}
	private int caseAuHasard(String joueur) {
		boolean caseVideTrouvée = false;
		int caseSélectionnée = -1;

		do {
			caseSélectionnée = (int) (Math.random() * 9);  
			if (cases[caseSélectionnée].getLabel().equals("")) {
				caseVideTrouvée = true;
			}
		} while (!caseVideTrouvée);

		return caseSélectionnée;
	}
	private int findCaseEmpty(String joueur) {
		int poids[] = new int[9];

		for (int i = 0; i < 9; i++ ) {
			if (cases[i].getLabel().equals("O"))
				poids[i] = -1;
			else if (cases[i].getLabel().equals("X"))
				poids[i] = 1;
			else
				poids[i] = 0;
		}

		int deuxPoids = joueur.equals("O") ? -2 : 2;

		if (poids[0] + poids[1] + poids[2] == deuxPoids) {
			if (poids[0] == 0)
				return 0;
			else if (poids[1] == 0)
				return 1;
			else
				return 2;
		}

		if (poids[3] + poids[4] + poids[5] == deuxPoids) {
			if (poids[3] == 0)
				return 3;
			else if (poids[4] == 0)
				return 4;
			else
				return 5;
		}

		if (poids[6] + poids[7] + poids[8] == deuxPoids) {
			if (poids[6] == 0)
				return 6;
			else if (poids[7] == 0)
				return 7;
			else
				return 8;
		}
		if (poids[0] + poids[3] + poids[6] == deuxPoids) {
			if (poids[0] == 0)
				return 0;
			else if (poids[3] == 0)
				return 3;
			else
				return 6;
		}
		if (poids[1] + poids[4] + poids[7] == deuxPoids) {
			if (poids[1] == 0)
				return 1;
			else if (poids[4] == 0)
				return 4;
			else
				return 7;
		}
		if (poids[2] + poids[5] + poids[8] == deuxPoids) {
			if (poids[2] == 0)
				return 2;
			else if (poids[5] == 0)
				return 5;
			else
				return 8;
		}
		if (poids[0] + poids[4] + poids[8] == deuxPoids) {
			if (poids[0] == 0)
				return 0;
			else if (poids[4] == 0)
				return 4;
			else
				return 8;
		}
		if (poids[2] + poids[4] + poids[6] == deuxPoids) {
			if (poids[2] == 0)
				return 2;
			else if (poids[4] == 0)
				return 4;
			else
				return 6;
		}

		return -1;
	}
	private void finishGame() {
		newGame.setEnabled(true);
		for (int i = 0; i < 9; i++) {
			cases[i].setEnabled(false);
		} 
	}
	private String whoIsWinner() {
		String winner ="";
		casesFree -- ;

		if (!cases[0].getLabel().equals("") &&
				cases[0].getLabel().equals(cases[1].getLabel()) &&
				cases[0].getLabel().equals(cases[2].getLabel())) {
			winner = cases[0].getLabel();
			showWinner(0, 1, 2);

		} else if (!cases[3].getLabel().equals("") && 
				cases[3].getLabel().equals(cases[4].getLabel()) &&
				cases[3].getLabel().equals(cases[5].getLabel())) {
			winner = cases[3].getLabel();
			showWinner(3, 4, 5);

		} else if (!cases[6].getLabel().equals("") && 
				cases[6].getLabel().equals(cases[7].getLabel()) &&
				cases[6].getLabel().equals(cases[8].getLabel())) {
			winner = cases[6].getLabel();
			showWinner(6, 7, 8);

		} else if (!cases[0].getLabel().equals("") && 
				cases[0].getLabel().equals(cases[3].getLabel()) &&
				cases[0].getLabel().equals(cases[6].getLabel())) {
			winner  = cases[0].getLabel();
			showWinner(0, 3, 6);

		} else if (!cases[1].getLabel().equals("") && 
				cases[1].getLabel().equals(cases[4].getLabel()) &&
				cases[1].getLabel().equals(cases[7].getLabel())) {
			winner  = cases[1].getLabel();
			showWinner(1, 4, 7);

		} else if (!cases[2].getLabel().equals("") && 
				cases[2].getLabel().equals(cases[5].getLabel()) &&
				cases[2].getLabel().equals(cases[8].getLabel())) {
			winner  = cases[2].getLabel();
			showWinner(2, 5, 8);

		} else if (!cases[0].getLabel().equals("") && 
				cases[0].getLabel().equals(cases[4].getLabel()) &&
				cases[0].getLabel().equals(cases[8].getLabel())) {
			winner  = cases[0].getLabel();
			showWinner(0, 4, 8);

		} else if (!cases[2].getLabel().equals("") && 
				cases[2].getLabel().equals(cases[4].getLabel()) &&
				cases[2].getLabel().equals(cases[6].getLabel())) {
			winner  = cases[2].getLabel();
			showWinner(2, 4, 6);
		} else if (casesFree == 0) {
			return "T";  // Partie nulle
		}

		return  winner ;
	}
	private void showWinner(int winner1, int winner2, int winner3) {
		cases[winner1].setBackground(Color.RED);
		cases[winner2].setBackground(Color.RED);
		cases[winner3].setBackground(Color.RED);
	}

}

